package com.ushakov.tm.api.service;

import com.ushakov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

public interface EndpointLocator {

    @NotNull
    AdminDataEndpoint getAdminDataEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    Session getSession();

    void setSession(Session session);

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

}
