package com.ushakov.tm.bootstrap;

import com.ushakov.tm.api.repository.ICommandRepository;
import com.ushakov.tm.api.service.EndpointLocator;
import com.ushakov.tm.api.service.ICommandService;
import com.ushakov.tm.api.service.ILoggerService;
import com.ushakov.tm.api.service.IPropertyService;
import com.ushakov.tm.command.AbstractCommand;
import com.ushakov.tm.component.FileScanner;
import com.ushakov.tm.endpoint.*;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.system.UnknownCommandException;
import com.ushakov.tm.repository.CommandRepository;
import com.ushakov.tm.service.CommandService;
import com.ushakov.tm.service.LoggerService;
import com.ushakov.tm.service.PropertyService;
import com.ushakov.tm.util.SystemUtil;
import com.ushakov.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Getter
@Setter
public class Bootstrap implements EndpointLocator {

    @NotNull
    private final List<AbstractCommand> arguments = new ArrayList<>();

    @NotNull
    private final List<AbstractCommand> commands = new ArrayList<>();

    @NotNull
    private final AdminDataEndpointService adminDataEndpointService = new AdminDataEndpointService();

    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = adminDataEndpointService.getAdminDataEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @NotNull
    public FileScanner fileScanner = new FileScanner(this);
    @NotNull
    private Session session;

    @NotNull
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @SneakyThrows
    public void initCommands() {
        @NotNull final Reflections reflections = new Reflections("com.ushakov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(com.ushakov.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            try {
                registry(clazz.newInstance());
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
            }
        }
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void parseArg(@Nullable final String arg) {
        if (!Optional.ofNullable(arg).isPresent() || arg.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        @Nullable final Role[] roles = command.roles();
        command.execute();
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String cmd) {
        if (!Optional.ofNullable(cmd).isPresent() || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        command.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setEndpointLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String... args) {
        loggerService.debug("EXAMPLE");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        fileScanner.init();
        while (true) {
            System.out.println("ENTER COMMAND");
            @Nullable final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[SUCCESS]\n");
            } catch (Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]\n");
            }
        }
    }

}
