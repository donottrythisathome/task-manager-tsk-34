package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.endpoint.Task;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("ENTER TASK NAME");
        @NotNull final String taskName = TerminalUtil.nextLine();
        @Nullable final Task task = endpointLocator.getTaskEndpoint().removeTaskByName(session, taskName);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

    @Override
    @NotNull
    public String name() {
        return "remove-task-by-name";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
