package com.ushakov.tm.command.auth;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public class UserRegistrationCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Register new user.";
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTER]:");
        System.out.println("ENTER LOGIN:");
        @NotNull final Session session = endpointLocator.getSession();
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        endpointLocator.getAdminUserEndpoint().createWithEmail(session, login, password, email);
    }

    @Override
    @NotNull
    public String name() {
        return "user-registration";
    }

}
