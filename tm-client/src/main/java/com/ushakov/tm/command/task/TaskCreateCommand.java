package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String taskDescription = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().createTask(session, taskName, taskDescription);
    }

    @Override
    @NotNull
    public String name() {
        return "task-create";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
