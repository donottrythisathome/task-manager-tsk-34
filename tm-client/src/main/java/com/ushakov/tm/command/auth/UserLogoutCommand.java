package com.ushakov.tm.command.auth;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.endpoint.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserLogoutCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Logout as a current user.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]:");
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getSessionEndpoint().closeSession(session);
        endpointLocator.setSession(null);
        System.out.println("SUCCESS");
    }

    @Override
    @NotNull
    public String name() {
        return "logout";
    }

}
