package com.ushakov.tm.command.data.backup;

import com.ushakov.tm.command.AbstractDataCommand;
import com.ushakov.tm.endpoint.Session;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class BackupSaveCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Backup domain data.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminDataEndpoint().saveBackup(session);
    }

    @Override
    @NotNull
    public String name() {
        return "backup-save";
    }

}
