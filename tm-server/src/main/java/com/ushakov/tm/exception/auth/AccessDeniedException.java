package com.ushakov.tm.exception.auth;

import com.ushakov.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access is denied");
    }

}
