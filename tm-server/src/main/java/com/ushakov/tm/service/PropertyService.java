package com.ushakov.tm.service;

import com.jcabi.manifests.Manifests;
import com.ushakov.tm.api.service.IPropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String SERVER_HOST = "server.host";

    @NotNull
    private static final String DEFAULT_SERVER_HOST = "localhost";

    @NotNull
    private static final String SERVER_PORT = "server.port";

    @NotNull
    private static final String DEFAULT_SERVER_PORT = "8080";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String APPLICATION_DEVELOPER_KEY = "developer.name";

    @NotNull
    private static final String APPLICATION_DEVELOPER_DEFAULT = "Ivan Ushakov";

    @NotNull
    private static final String DEVELOPER_EMAIL_KEY = "developer.email";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "iushakov@t1-consulting.com";

    @NotNull
    private static final String SESSION_SECRET_KEY = "session.secret";

    @NotNull
    private static final String SESSION_SECRET_DEFAULT = "";

    @NotNull
    private static final String SESSION_ITERATION_KEY = "session.iteration";

    @NotNull
    private static final String SESSION_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String BACKUP_INTERVAL_KEY = "backup.interval";

    @NotNull
    private static final String BACKUP_INTERVAL_DEFAULT = "30";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getManifestValue("build", APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getBackupInterval() {
        return getInteger(BACKUP_INTERVAL_KEY, BACKUP_INTERVAL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return getManifestValue("email", DEVELOPER_EMAIL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return getManifestValue("developer", APPLICATION_DEVELOPER_DEFAULT);
    }

    public Integer getInteger(@NotNull final String name, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(name)) {
            final String value = System.getProperty(name);
            return Integer.parseInt(value);
        }
        if (System.getenv().containsKey(name)) {
            final String value = System.getenv(name);
            return Integer.parseInt(value);
        }
        final String value = properties.getProperty(name, defaultValue);
        return Integer.parseInt(value);
    }

    @NotNull
    private String getManifestValue(@NotNull final String manifest, @NotNull final String defaultValue) {
        if (Manifests.exists(manifest)) return Manifests.read(manifest);
        return defaultValue;
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(PASSWORD_ITERATION_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT));
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getValue(SERVER_HOST, DEFAULT_SERVER_HOST);
    }

    @NotNull
    @Override
    public String getServerPort() {
        return getValue(SERVER_PORT, DEFAULT_SERVER_PORT);
    }

    @NotNull
    @Override
    public Integer getSessionIteration() {
        return getInteger(SESSION_ITERATION_KEY, SESSION_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionSecret() {
        return getValue(SESSION_SECRET_KEY, SESSION_SECRET_DEFAULT);
    }

    @NotNull
    private String getValue(@NotNull final String name, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(name)) return System.getProperty(name);
        if (System.getenv().containsKey(name)) return System.getProperty(name);
        return properties.getProperty(name, defaultValue);
    }

}
