package com.ushakov.tm.endpoint;

import com.ushakov.tm.api.endpoint.ITaskEndpoint;
import com.ushakov.tm.api.service.IProjectTaskService;
import com.ushakov.tm.api.service.ISessionService;
import com.ushakov.tm.api.service.ITaskService;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Session;
import com.ushakov.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    private ITaskService taskService;

    private ISessionService sessionService;

    private IProjectTaskService projectTaskService;

    public TaskEndpoint() {
    }

    public TaskEndpoint(final ITaskService taskService, ISessionService sessionService, IProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.sessionService = sessionService;
        this.projectTaskService = projectTaskService;
    }

    public TaskEndpoint(ITaskService taskService, ISessionService sessionService) {
        super();
    }

    @Override
    @WebMethod
    public void addAllTasks(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entities") List<Task> entities
    ) {
        sessionService.validate(session);
        taskService.addAll(session.getUserId(), entities);
    }

    @Override
    @WebMethod
    public void addTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") Task entity
    ) {
        sessionService.validate(session);
        taskService.add(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public @NotNull Task changeTaskStatusById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    ) {
        sessionService.validate(session);
        return taskService.changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public @NotNull Task changeTaskStatusByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    ) {
        sessionService.validate(session);
        return taskService.changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    public @NotNull Task changeTaskStatusByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    ) {
        sessionService.validate(session);
        return taskService.changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public void clearTasks(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task completeTaskById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return taskService.completeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Task completeTaskByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return taskService.completeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Task completeTaskByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return taskService.completeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createTask(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        taskService.add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    @Nullable
    public List<Task> findAllTasks(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session);
        return taskService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task findTaskById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return taskService.findOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @Nullable Task findTaskByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return taskService.findOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Task findTaskByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return taskService.findOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @Nullable Task removeTaskById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return taskService.removeOneById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @Nullable Task removeTaskByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return taskService.removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    @Nullable
    public Task removeTaskByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return taskService.removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @NotNull Task startTaskById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return taskService.startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Task startTaskByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    ) {
        sessionService.validate(session);
        return taskService.startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public @NotNull Task startTaskByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    ) {
        sessionService.validate(session);
        return taskService.startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @Nullable Task unbindTaskFromProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "taskId") final String taskId
    ) {
        sessionService.validate(session);
        return projectTaskService.unbindTaskFromProject(session.getUserId(), taskId);
    }

    @Override
    @WebMethod
    public void updateTaskById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        taskService.updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateTaskByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    ) {
        sessionService.validate(session);
        taskService.updateByIndex(session.getUserId(), index, name, description);
    }

}
