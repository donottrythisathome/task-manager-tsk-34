package com.ushakov.tm.endpoint;

import com.ushakov.tm.api.endpoint.IAdminUserEndpoint;
import com.ushakov.tm.api.service.ISessionService;
import com.ushakov.tm.api.service.IUserService;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.model.Session;
import com.ushakov.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    private IUserService userService;

    private ISessionService sessionService;

    public AdminUserEndpoint() {
    }

    public AdminUserEndpoint(@NotNull final IUserService userService, ISessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void addAllUsers(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entities") List<User> entities
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.addAll(entities);
    }

    @Override
    @WebMethod
    public void addUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") User entity
    ) {

        sessionService.validate(session, Role.ADMIN);
        userService.add(entity);
    }

    @Override
    @WebMethod
    public void clearUsers(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session, Role.ADMIN);
        userService.clear();
    }

    @Override
    @WebMethod
    public @NotNull User createUser(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String userLogin,
            @Nullable @WebParam(name = "password") String password
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.add(userLogin, password);
    }

    @Override
    @WebMethod
    public @NotNull User createWithEmail(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String userLogin,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "email") String email
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.add(userLogin, password, email);
    }

    @Override
    @WebMethod
    public @NotNull User createWithRole(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String userLogin,
            @Nullable @WebParam(name = "password") String password,
            @Nullable @WebParam(name = "role") Role role
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.add(userLogin, password, role);
    }

    @Override
    @WebMethod
    public @Nullable List<User> findAllUsers(@Nullable @WebParam(name = "session") Session session) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findAll();
    }

    @Override
    @WebMethod
    @Nullable
    public User findUserById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findOneByLogin(id);
    }

    @Override
    @WebMethod
    public @Nullable User findUserByLogin(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "login") String userLogin
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.findOneByLogin(userLogin);
    }

    @Override
    @WebMethod
    public @NotNull User lockUserByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.lockByLogin(login);
    }


    @Override
    @WebMethod
    public void removeUser(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") User entity
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.remove(entity);
    }

    @Override
    @WebMethod
    public @Nullable User removeUserById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeOneById(id);
    }

    @Override
    @WebMethod
    public @NotNull User removeUserByLogin(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "login") String userLogin
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.removeOneByLogin(userLogin);
    }

    @Override
    @WebMethod
    public void setUserPassword(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "password") String password
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.setPassword(id, password);
    }

    @Override
    @WebMethod
    public @NotNull User unlockUserByLogin(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.unlockByLogin(login);
    }


    @Override
    @WebMethod
    public void updateUserById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "firstName", partName = "firstName") final String firstName,
            @WebParam(name = "lastName", partName = "lastName") final String lastName,
            @WebParam(name = "middleName", partName = "middleName") final String middleName
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.updateUser(session.getUserId(), firstName, lastName, middleName);
    }

}
