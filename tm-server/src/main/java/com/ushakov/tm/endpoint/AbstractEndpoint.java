package com.ushakov.tm.endpoint;

import lombok.AccessLevel;
import lombok.Getter;

@Getter(value = AccessLevel.PROTECTED)
public abstract class AbstractEndpoint {
}
