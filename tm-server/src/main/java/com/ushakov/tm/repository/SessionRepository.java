package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.ISessionRepository;
import com.ushakov.tm.model.Session;
import org.jetbrains.annotations.NotNull;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public boolean contains(@NotNull final String id) {
        return list.stream().anyMatch(e -> id.equals(e.getId()));
    }

}
